package com.adamtoczek.functional_patterns.observers;

import java.util.Observable;

/**
 * Created by Adam Toczek on 08.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */

public class ObservableTempValue extends Observable {
    private int oldTemp = 0;

    public void setValue(int newTemp) {
        if (newTemp != oldTemp) {
            super.setChanged();
            super.notifyObservers(newTemp);
            oldTemp = newTemp;
        }
    }

    public int getOldTemp() {
        return oldTemp;
    }
}
