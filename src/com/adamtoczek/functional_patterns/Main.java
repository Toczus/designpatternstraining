package com.adamtoczek.functional_patterns;

import com.adamtoczek.functional_patterns.observers.ObservableTempValue;
import com.adamtoczek.functional_patterns.strategy.BikeTravelStrategy;
import com.adamtoczek.functional_patterns.strategy.CarTravelStrategy;
import com.adamtoczek.functional_patterns.strategy.DoctorJobStrategy;
import com.adamtoczek.functional_patterns.strategy.Employee;
import com.adamtoczek.functional_patterns.strategy.SandwichBreakfastStrategy;

/**
 * Created by Adam Toczek on 08.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
class Main {

    public static void main(String[] args) throws InterruptedException {

        observerPattern();

        strategyPattern();

    }

    private static void strategyPattern() {
        // replaces Enum / switch case with logic

        Employee mike = new Employee();

        mike.travelStrategy = new BikeTravelStrategy();
        mike.jobStrategy = new DoctorJobStrategy();
        mike.breakfastStrategy = new SandwichBreakfastStrategy();

        mike.goToWork();
        mike.doYourJob();
        mike.eatYourBreakfast(mike);

        mike.travelStrategy = new CarTravelStrategy();
    }

    private static void observerPattern() throws InterruptedException {
        // User interface operation
        // Handling of repository changes
        // Handle changes to sensors

        // CERTAINLY we are NOT querying the observed object
        // The observed object notifies the interested objects of the change

        ObservableTempValue observableValue = new ObservableTempValue();

        observableValue.addObserver((observable, o) -> System.out.println("1 " + o.toString()));
        observableValue.addObserver((observable, o) -> System.out.println("2 " + o.toString()));

//        while (true) { todo uncoment if you want test Observers
//            Thread.sleep(500);
//            observableValue.setValue((int) (observableValue.getOldTemp() + Math.random() * 3 - 1));
//        }
    }
}
