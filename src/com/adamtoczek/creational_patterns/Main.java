package com.adamtoczek.creational_patterns;

import com.adamtoczek.creational_patterns.models.FamilyHouse;

class Main {

    public static void main(String[] args) {

        creationPatterns();
    }


    private static void creationPatterns() {
        //BUILDER
        //new House("ul. Kursowa 2", 2, 3, 12);
        // new House .setAdress .setFloorsNumber .setDoorsNumber .setWindowsNumber
        House house = new House.HouseBuilder()
                .setAdress("ul. Szkolna 2")
                .setDoorsNumber(3)
                .setWindowsNumer(10)
                .build();

        //STATIC MANUFACTURING METHODS
        //from, of, valueOf, instanceOf
        boolean isTrue = true;
        Boolean.valueOf(isTrue);

        FamilyHouse familyHouse = FamilyHouse.from(house);
    }
}
