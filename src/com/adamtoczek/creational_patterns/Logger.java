package com.adamtoczek.creational_patterns;

/**
 * Created by Adam Toczek on 07.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class Logger {

    private static Logger instance;

    private Logger() {
    }

    public static Logger getInstance() {
        return SingletonHolder.INSTANCE;
        // By great Bill Pugh :)
    }

    public void logToConsole() {
        //
    }

    private static class SingletonHolder {
        private static final Logger INSTANCE = new Logger();
    }
}
