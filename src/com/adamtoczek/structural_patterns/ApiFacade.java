package com.adamtoczek.structural_patterns;

import com.adamtoczek.structural_patterns.database.EmployeeCreator;
import com.adamtoczek.structural_patterns.database.EmployeeDatabase;
import com.adamtoczek.structural_patterns.decorator.FreqBonus;
import com.adamtoczek.structural_patterns.decorator.Payable;
import com.adamtoczek.structural_patterns.decorator.SpecialBonus;
import com.adamtoczek.structural_patterns.strategy.BikeTravelStrategy;
import com.adamtoczek.structural_patterns.strategy.CarTravelStrategy;
import com.adamtoczek.structural_patterns.strategy.Employee;

import static com.adamtoczek.structural_patterns.database.EmployeeCreator.BIKE_DOCTOR_SANDWITCH;

/**
 * Created by Adam Toczek on 11.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */

class ApiFacade {

    private EmployeeDatabase eDataBase = new EmployeeDatabase();
    private EmployeeCreator employeeCreator = new EmployeeCreator();

    public Employee createDoctor(final int i) {
        employeeCreator = new EmployeeCreator();
        Employee mike = employeeCreator.create(BIKE_DOCTOR_SANDWITCH);
        mike.setSalary(i);

        eDataBase.addEmployee(mike);
        return mike;
    }

    public void pushDoctorToJob(final Employee mike) {
        mike.goToWork();
        mike.doYourJob();
        mike.eatYourBreakfast();

        mike.travelStrategy = new CarTravelStrategy();
        mike.goToWork();

    }

    public int countSalary(final Employee mike) {
        Payable employee = mike;
        if (mike.getSalary() > 8000) {
            employee = new SpecialBonus(employee);
        }
        if (mike.travelStrategy instanceof BikeTravelStrategy) {
            employee = new FreqBonus(employee);
        }
        return employee.getSalary();
    }

    public void giveFreeHouseToBestFreqEmployee(Employee mike) {
        System.out.println(mike.toString() + " dostaje dom " + new House.HouseBuilder()
                .setAdress("ul. Sliwkowa 2")
                .setDoorsNumber(3)
                .setWindowsNumer(10)
                .setFloorsNumer(2)
                .build()
                .toString());
    }
}
