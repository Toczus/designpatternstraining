package com.adamtoczek.structural_patterns.strategy;

/**
 * Created by Adam Toczek on 08.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public interface BreakfastStrategy {

    void eatYourBreakfast();
}
