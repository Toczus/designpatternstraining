package com.adamtoczek.structural_patterns.adapter;

import com.adamtoczek.structural_patterns.strategy.Employee;

/**
 * Created by Adam Toczek on 08.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class OfficialTrippingEmployee {

    private Employee employee;

    public OfficialTrippingEmployee(final Employee employee) {
        this.employee = employee;
    }

    public void goToClient() {
        employee.goToWork();
        System.out.println(" to client");
    }
}
