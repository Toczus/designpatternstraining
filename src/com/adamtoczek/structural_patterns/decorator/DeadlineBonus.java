package com.adamtoczek.structural_patterns.decorator;

/**
 * Created by Adam Toczek on 11.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class DeadlineBonus extends Bonus {
    public DeadlineBonus(final Payable payable) {
        super(payable);
    }

    @Override
    protected int getPaidBonus(final int salary) {
        return (int) (salary * 0.1f);
    }
}
