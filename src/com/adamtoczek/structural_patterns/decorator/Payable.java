package com.adamtoczek.structural_patterns.decorator;

/**
 * Created by Adam Toczek on 11.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */

public interface Payable {
    int getSalary();
}
