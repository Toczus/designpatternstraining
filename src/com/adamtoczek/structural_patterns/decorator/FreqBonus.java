package com.adamtoczek.structural_patterns.decorator;

/**
 * Created by Adam Toczek on 11.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class FreqBonus extends Bonus {
    public FreqBonus(Payable payable) {
        super(payable);
    }

    @Override
    protected int getPaidBonus(final int salary) {
        return 100;
    }
}
