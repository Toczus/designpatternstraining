package com.adamtoczek.structural_patterns.decorator;

/**
 * Created by Adam Toczek on 11.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public abstract class Bonus implements Payable{

    private Payable payable;

    Bonus(Payable payable){
        this.payable = payable;
    }

    @Override
    public int getSalary() {
        return payable.getSalary() + getPaidBonus(payable.getSalary());
    }

    abstract protected int getPaidBonus(int salary);
}
