package com.adamtoczek.structural_patterns.database;

import com.adamtoczek.structural_patterns.strategy.Employee;

import java.util.ArrayList;

/**
 * Created by Adam Toczek on 11.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class EmployeeDatabase {

    private ArrayList<Employee> employeeList = new ArrayList<>();

    {
        employeeList.add(new Employee());
        employeeList.add(new Employee());
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }

    public void addEmployee(Employee employee) {
        this.employeeList.add(employee);
    }
}
