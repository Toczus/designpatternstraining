package com.adamtoczek.structural_patterns.database;

import com.adamtoczek.structural_patterns.strategy.BikeTravelStrategy;
import com.adamtoczek.structural_patterns.strategy.DoctorJobStrategy;
import com.adamtoczek.structural_patterns.strategy.Employee;
import com.adamtoczek.structural_patterns.strategy.SandwichBreakfastStrategy;

/**
 * Created by Adam Toczek on 11.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class EmployeeCreator {

    public static final String BIKE_DOCTOR_SANDWITCH = "bikeDoctorSandwitch";

    public Employee create(String employeeType) {
        switch(employeeType) {
            case BIKE_DOCTOR_SANDWITCH:
            {
                Employee mike = new Employee();

                mike.travelStrategy = new BikeTravelStrategy();
                mike.jobStrategy = new DoctorJobStrategy();
                mike.breakfastStrategy = new SandwichBreakfastStrategy();

                return mike;
            }
            default: return new Employee();
        }
    }
}
