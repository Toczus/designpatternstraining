package com.adamtoczek.structural_patterns.models;

import com.adamtoczek.structural_patterns.House;

/**
 * Created by Adam Toczek on 07.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class FamilyHouse {

    String adress;
    Integer floorsNumer;

    public FamilyHouse(String adress, Integer floorsNumer) {
        this.adress = adress;
        this.floorsNumer = floorsNumer;
    }

    public static FamilyHouse from(House house) {
        return new FamilyHouse(house.getAdress(), house.getFloorsNumer());
    }
}
