package com.adamtoczek.structural_patterns;

import com.adamtoczek.structural_patterns.strategy.Employee;

/**
 * Created by Adam Toczek on 08.01.2021.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */


class Main {

    public static void main(String[] args) throws InterruptedException {

        ApiFacade facade = new ApiFacade();
        Employee mike = facade.createDoctor(10000);
        facade.pushDoctorToJob(mike);
        facade.countSalary(mike);
        System.out.println("Zarobki " + facade.countSalary(mike));
        facade.giveFreeHouseToBestFreqEmployee(mike);
    }
}
